import { Link } from "react-router-dom";
import "./Navbar.css";
import { navItems } from "./Navitems.jsx";
import logo from '../assets/Image/logo.png'

function Navbar() {
  

  return (
    <>
      <nav className="navbar">
        <Link to="/" className="navbar-logo" >
          
          <span className="nav-title"><img src={logo} height={60} width={60} /> Optira </span>
        </Link>
        { (
          <ul className="nav-items">
            {navItems.map((item) => {
              return (
                <li key={item.id} className={item.nName}>
                  <Link to={item.path}>
                    {item.icon}
                  </Link>
                </li>
              );
            })}
          </ul>
        )}
        
      </nav>

      
    </>
  );
}

export default Navbar;
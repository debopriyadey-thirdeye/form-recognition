import * as Icons from "react-icons/fa";

export const SideItems = [
 
  {
    id: 1,
    // title: "Services",
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaHospital size={60} />,
  },
  {
    id: 2,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaDashcube size={60} />,
  },
  {
    id: 3,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaGlobe size={60} />,
  },
  {
    id: 4,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaOpencart size={60} />,
  },
  {
    id: 5,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaInnosoft size={60} />,
  },
  {
    id: 6,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaFileVideo size={60} />,
  },
  {
    id: 7,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaDribbbleSquare size={60} />,
  },
  {
    id: 8,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaKiwiBird size={60} />,
  },
  {
    id: 9,
    path: "./",
    sName: "sidebar-item",
    icon: <Icons.FaShareSquare size={60} />,
  },
 
];
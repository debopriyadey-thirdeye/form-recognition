// in ActionProvider.jsx
import React, { useState } from "react";
const { Configuration, OpenAIApi } = require("openai");

const ActionProvider = ({ createChatBotMessage, setState, children }) => {
  const configuration = new Configuration({
    apiKey: "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",
  });
  
  const openai = new OpenAIApi(configuration);
  const [prompt, setPrompt] = useState("");
  const [apiResponse, setApiResponse] = useState("");
  const [loading, setLoading] = useState(false);

  const handleHello = async (message) => {
    let botMessage = ""
    // console.log(message)
    try{
      var msg = sessionStorage.getItem('responce')
      var q = message
      let t = `give answers based on the following information:${msg} if the question is out of context, not in information say 'i dont know' Question:${q}.`;
      // const result = await openai.createCompletion({
      //   model: "text-davinci-003",
      //   prompt: t,
      //   // temperature: 0.5,
      //   //   max_tokens: 25000,
      //   max_tokens: 100,
      //   temperature: 1,
      //   frequency_penalty: 0,
      //   presence_penalty: 0,
      //   top_p: 0.5,
      //   best_of: 1,
      //   stop: null
      // });

      openai.createCompletion({
        model: "text-davinci-003",
        prompt: t,
        // temperature: 0.5,
        //   max_tokens: 25000,
        max_tokens: 100,
        temperature: 1,
        frequency_penalty: 0,
        presence_penalty: 0,
        top_p: 0.5,
        best_of: 1,
        stop: null
      }).then((result) => {
        setApiResponse(result.data.choices[0].text);
        botMessage = createChatBotMessage(result.data.choices[0].text);
  
        setState(prev => ({
          ...prev,
          messages: [...prev.messages, botMessage]
        }));
      })
      //console.log("response", result.data.choices[0].text);

    } catch (e) {
      //console.log(e);
      setApiResponse("Something is going wrong, Please try again.");
    }
    setLoading(false);
    // console.log(apiResponse)
    
  };

  // Put the handleHello function in the actions object to pass to the MessageParser
  return (
    <div>
      {React.Children.map(children, child => {
        return React.cloneElement(child, {
          actions: {
            handleHello
          }
        });
      })}
    </div>
  );
};

export default ActionProvider;

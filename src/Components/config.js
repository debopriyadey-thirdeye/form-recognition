import { createChatBotMessage } from 'react-chatbot-kit';

const config = {
  initialMessages: [createChatBotMessage(`You are interacting with Azure OpenAI Service in this chat.`)],
};

export default config;
import React, { useState } from "react";
import { CSVLink } from "react-csv";
import "./MainArea.css";
import MainUpperSection from "./MainUpperArea/MainUpperSection";
import ApiSection from "./ApiSection/ApiSection";
import { Button } from "@mui/material";
import { Document, Page, pdfjs } from "react-pdf";

import Modal from "../Model/Modal.mjs";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;


export default function MainArea() {
  const [currentImage, setCurrentImage] = useState();
  const [imageState, setImageState] = useState(false);
  const [apiResponse, setApiResponse] = useState();
  const [option, setOption] = useState("Hdfc_Statements");
  const [fileName, setFileName] = useState("");

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [data, setData] = useState([]);
  const [csvData, setCsvData] = useState([]);
  const [csvHeader, setCsvHeader] = useState([]);
  const [processClicked, setProcessClicked] = useState(false)
  React.useEffect(
    () => {
      setCsvHeader([]);
      if (data.length) {
        Object.keys(data[0]).forEach(el => {
          if (el == "Statement of Account") console.log("got it")
          setCsvHeader(header => {
            return [...header, { label: el, key: el }];
          });
        });
        setCsvData(CSVdata => {
          // if (!CSVdata.length) return [Object.values(data[0])];
          const obj = {};
          // data.forEach((data) => {
          //   Object.keys(data).forEach((el, i) => {
          //     obj[`${el}`] = Object.values(data[0])[i];
          //     // setCsvHeader((header) => {
          //     //   return [...header, { label: el, key: el }];
          //     // });
          //   });
          // });
          Object.keys(data[0]).forEach((el, i) => {
            obj[`${el}`] = "";
          });
          data.forEach(data1 => {
            CSVdata.push(data1);
            CSVdata.push(obj);
          });
          // console.log("object", obj);
          return [...CSVdata];
        });
      }
    },
    [data]
  );

  // const handleImageDisplay=(value)=>{
  //     setImageState(false)
  //     if(value==="contract2"){
  //         setCurrentImage(contract2)
  //     }else if(value==="contract3"){
  //         setCurrentImage(contract3)
  //     }else if(value==="contract4"){
  //         setCurrentImage(contract4)
  //     }else if(value==="contract5"){
  //         setCurrentImage(contract5)
  //     }else if(value==="Govind Beriwal-Jb Contract Note"){
  //       setCurrentImage(GOVIND_BERIWAL_JB_Contract_Note)
  //   }

  //   else if(value==="hdfc_Statement1"){
  //     setCurrentImage(statement1)
  //   }else if(value==="hdfc_Statement2"){
  //     setCurrentImage(statement2)
  //   }else if(value==="hdfc_Statement3"){
  //     setCurrentImage(statement3)
  //   }else if(value==="hdfc_Statement4"){
  //     setCurrentImage(statement4)
  //   }else if(value==="hdfc_Statement5"){
  //     setCurrentImage(statement5)
  //   }else if(value==="Anuj Beriwala-Iifl Bank Statement"){
  //     setCurrentImage(Anju_Beriwala_IIFL_Bank_Statement)
  //   }

  //   else if(value==="kotak_Statement2"){
  //     setCurrentImage(Kotak_Bank_Statement2)
  //   }else if(value==="kotak_Statement3"){
  //     setCurrentImage(Kotak_Bank_Statement3)
  //   }else if(value==="kotak_Statement4"){
  //     setCurrentImage(Kotak_Bank_Statement4)
  //   }else if(value==="kotak_Statement5"){
  //     setCurrentImage(Kotak_Bank_Statement5)
  //   }else if(value==="kotak_Statement6"){
  //     setCurrentImage(Kotak_Bank_Statement6)
  //   }

  // }

  const handleImageDisplay = value => {
    setOption(value);
  };

  const [files, setFiles] = useState([]);

  const handleUpload = e => {
    setFileName(e.target.files[0].name);
    let seletedFile = e.target.files[0];
    setCurrentImage(seletedFile);
  };

  const handleProcess = async () => {
    setProcessClicked(true)
    setData([])
    const response = await Modal(option, fileName);
    setProcessClicked(false)
    setApiResponse(response);
    sessionStorage.setItem('responce', JSON.stringify(response))
    setData(data => {
      return [...data, response];
    });
  };

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setPageNumber(1);
    console.log("Load doc success");
  }

  function changePage(offSet) {
    setPageNumber(prevPageNumber => prevPageNumber + offSet);
  }
  function changePageBack() {
    changePage(-1);
  }

  function changePageNext() {
    changePage(+1);
  }

  return (
    <div className="MainAreaBox">
      <MainUpperSection handleImageDisplay={handleImageDisplay} />
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-5">
            <div className="LeftSectionInner">
              <div className="LeftDocumentImage">
                <Document
                  file={currentImage}
                  onLoadSuccess={onDocumentLoadSuccess}
                >
                  <Page height={450} pageNumber={pageNumber} />
                </Document>
                {/* <object
              data={currentImage}
              type="application/pdf"
              width="100%"
              height="500px"
            >
              {" "}
            </object> */}
              </div>
              <div className="ImageButtons">
                <Button
                  // type="file"
                  sx={{
                    borderRadius: "8px",
                    color: "black",
                    backgroundColor: "white"
                  }}
                >
                  {" "}<input
                    className="custom-file-input"
                    type="file"
                    onChange={handleUpload}
                  />
                </Button>

                <Button
                  sx={{
                    borderRadius: "8px",
                    color: "black",
                    backgroundColor: "white"
                  }}
                  variant="contained"
                  onClick={handleProcess}
                >
                  {" "}Process
                </Button>
              </div>
            </div>
          </div>
          <div className="RightSection col-md-12 col-lg-7 d-flex align-items-center">
            <div className="RightSectionInner">
              <ApiSection processClicked={processClicked} apiResponse={apiResponse} />
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "50%"
                }}
              >
                {data.length
                  ? <CSVLink data={csvData} headers={csvHeader} style={{}}>
                      Download CSV
                    </CSVLink>
                  : ""}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

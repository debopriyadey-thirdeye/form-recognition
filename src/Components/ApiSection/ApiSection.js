import React, { useEffect, useState } from "react";
import "./ApiSection.css";
import Chatbot from "react-chatbot-kit";
import "react-chatbot-kit/build/main.css";
import config from "../config";
import MessageParser from "../MessageParser";
import ActionProvider from "../ActionProvider";
import { Audio, Blocks } from "react-loader-spinner";
const { Configuration, OpenAIApi } = require("openai");

export default function ApiSection({ processClicked, apiResponse }) {
  
  return (
    <div className="ResultSec">
      {(processClicked) && 
        <Blocks
          visible={true}
          height="80"
          width="80"
          ariaLabel="blocks-loading"
          wrapperStyle={{}}
          wrapperClass="blocks-wrapper"
        />}
      {(!processClicked && apiResponse) &&
        <div className="ApiSectionBox">
          <div className="ApiSectionBoxLeft">
            <h3 className="pb-3">RESULT:</h3>

            {apiResponse &&
              Object.entries(apiResponse).map(([key, value]) => (
                  <div key={key}>
                    {
                      key == "Statement of Account" ?<><span style={{ fontWeight: "bold" }}>{key}</span>:<ul>
                       { Object.entries(value).map(([k, v]) => (
                        <li> {" "}<span style={{ fontWeight: "bold" }}>{k}</span>:{v ? v.toString() : ""}
                        </li>
                       ))}
                      </ul> </>: 
                      (<>
                        {" "}<span style={{ fontWeight: "bold" }}>{key}</span>:{value ? value.toString() : ""}
                      </>)
                    }
                  </div>
                ))}
          </div>
          <div className="ApiSectionBoxRight">
            <Chatbot
              config={config}
              messageParser={MessageParser}
              actionProvider={ActionProvider}
            />
          </div>
        </div>}
    </div>
  );
}
